#include "client3.h"

extern sem_t *print_sem;
extern sem_t *client3_sem;

extern sem_t *client3_write;
extern sem_t *client3_read;

void client3_func(void)
{
	//client3 member
	bool run = true;

	int client3_fd = -1;

	char *client3_buf = NULL;

	time_t t1 = 0, t2 = 0;

	size_t buf_size = 0;
	size_t buf_len = 0;
	char *recv_buf = NULL;

	size_t client3_recv_len = 0;

	uint64_t element_count = 0;
	int32_t *element_array = NULL;

	//client3 prepare
	time(&t1);

	while(time(&t2) <=  t1 + 1 && (client3_fd = shm_open("interview_shm", O_RDONLY, 0666)) == -1)
		;

	if(client3_fd == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client3 open file failed! %s\n", strerror(errno));

		sem_post(print_sem);

		return;
	}

	if((client3_buf = mmap(NULL, 4096, PROT_READ, MAP_SHARED, client3_fd, 0)) == NULL)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client3 buffer setting failed! %s\n", strerror(errno));

		sem_post(print_sem);

		return;
	}

	sem_wait(client3_sem);

	sem_wait(print_sem);

	fprintf(stdout, "client3 porecss ready\n");

	sem_post(print_sem);

	buf_size = 4096;
	buf_len = 0;
	recv_buf = calloc(1, buf_size);

	while(run)
	{
		sem_wait(client3_read);

		client3_recv_len = *(size_t *) &client3_buf[0];

		if(buf_len + client3_recv_len > buf_size)
		{
			buf_size += 4096;
			recv_buf = realloc(recv_buf, buf_size);
		}

		memcpy(&recv_buf[buf_len], &client3_buf[sizeof(size_t)], client3_recv_len);
		buf_len += client3_recv_len;

		sem_post(client3_write);

		while(true)
		{
			if(!memcmp(recv_buf, "quit", 4))
			{
				run = false;
				break;
			}

			if(!element_count && buf_len > sizeof(uint64_t))
				element_count = *(uint64_t *) recv_buf;

			if(element_count && buf_len >= sizeof(uint64_t) + element_count * sizeof(int32_t))
				element_array = (int32_t *) &((uint64_t *) recv_buf)[1];
			else
				break;

			if(element_array)
			{
				int32_t *count_array = calloc(element_count, sizeof(int32_t));
				int32_t *mode_array = calloc(element_count, sizeof(int32_t));

				uint64_t mode_count = 0, index = 0;

				for(uint64_t i = 0; i < element_count; i++)
					for(uint64_t j = i; j < element_count; j++)
						if(element_array[i] == element_array[j])
							count_array[i]++;


				mode_count = count_array[0];

				for(uint64_t i = 0; i < element_count; i++)
					if(mode_count <= count_array[i])
					{
						mode_count = count_array[i];
						index = i;
					}

				mode_array[index] = element_array[index];

				for(uint64_t i = 0; i < element_count; i++)
					if(mode_count == count_array[i])
						mode_array[i] = element_array[i];

				sem_wait(print_sem);

				fprintf(stdout, "client3: mode");
				for(uint64_t i = 0; i < element_count; i++)
					if(mode_array[i] != 0)
						fprintf(stdout, " %d", mode_array[i]);
				fprintf(stdout, "\n");

				//printf("count %lu\n", element_count);
				//for(uint64_t i = 0; i < element_count; i++)
				//	printf("%lu, %d\n", i, element_array[i]);
				//printf("\n");

				sem_post(print_sem);

				free(count_array);
				count_array = NULL;
				free(mode_array);
				mode_array = NULL;
			}

			if(buf_len == sizeof(uint64_t) + element_count * sizeof(int32_t))
			{
				free(recv_buf);
				recv_buf = NULL;

				buf_size = 4096;
				buf_len = 0;
				recv_buf = calloc(1, buf_size);

				element_count = 0;
				element_array = NULL;

				break;
			}
			else
			{
				size_t temp_len = buf_len - (sizeof(uint64_t) + element_count * sizeof(int32_t));
				char *temp_buf = calloc(1, temp_len);
				memcpy(temp_buf, &recv_buf[sizeof(uint64_t) + element_count * sizeof(int32_t)], temp_len);

				buf_size = buf_len = temp_len;
				free(recv_buf);
				recv_buf = temp_buf;
				temp_buf = NULL;

				element_count = 0;
				element_array = NULL;
			}
		}
	}

	munmap(client3_buf, 4096);
	close(client3_fd);
	client3_fd = -1;
	shm_unlink("interview_shm");

	sem_wait(print_sem);

	fprintf(stdout, "client3 porecss terminated\n");

	sem_post(print_sem);

	sem_post(client3_sem);

	return;
}
