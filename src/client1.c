#include "client1.h"

extern sem_t *print_sem;
extern sem_t *client1_sem;

void client1_func(void)
{
	//client1 member
	bool run = true;

	struct socket_set client_set;
	struct epoll_set epoll_obj;

	time_t t1 = 0, t2 = 0;

	size_t buf_size = 0;
	size_t buf_len = 0;
	char *recv_buf = NULL;

	ssize_t recv_result = 0;

	uint64_t element_count = 0;
	int32_t *element_array = NULL;

	//server member
	struct socket_set server_set;

	//server prepare
	socket_init(&server_set);

	server_set.remote_in.sin_family = AF_INET;
	server_set.remote_in.sin_port = htons(DEF_PORT);
	inet_pton(AF_INET, DEF_IP, &server_set.remote_in.sin_addr);

	//clienti1 prepare
	socket_init(&client_set);

	if(!epoll_init(&epoll_obj))
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client1 epoll create failed! %s\n", strerror(errno));

		sem_post(print_sem);

		return;
	}

	if((client_set.sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client1 socket create failed! %s\n", strerror(errno));

		sem_post(print_sem);

		epoll_dest(&epoll_obj);

		return;
	}

	if((epoll_obj.flag = fcntl(client_set.sock, F_GETFL, 0)) == -1 || fcntl(client_set.sock, F_SETFL, epoll_obj.flag |O_NONBLOCK ) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client1 socket set failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &client_set);
		epoll_dest(&epoll_obj);

		return;
	}

	memset(&epoll_obj.event, 0, sizeof(struct epoll_event));
	epoll_obj.event.events = EPOLLIN | EPOLLET;
	epoll_obj.event.data.fd = client_set.sock;

	if(epoll_ctl(epoll_obj.epoll, EPOLL_CTL_ADD, client_set.sock, &epoll_obj.event) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client1 epoll set failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &client_set);
		epoll_dest(&epoll_obj);

		return;
	}

	time(&t1);

	while(time(&t2) <=  t1 + 1 && connect(client_set.sock, (struct sockaddr *) &server_set.remote_in, server_set.sin_len) == -1)
		;

	if(client_set.sock == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client1 socket set failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &client_set);
		epoll_dest(&epoll_obj);

		return;
	}
	else
	{
		getsockname(client_set.sock, (struct sockaddr *) &client_set.local_in, &client_set.sin_len);
		getpeername(client_set.sock, (struct sockaddr *) &client_set.remote_in, &client_set.sin_len);
	}

	sem_wait(client1_sem);

	sem_wait(print_sem);

	fprintf(stdout, "client1 porecss ready\n");

	sem_post(print_sem);

	buf_size = 4096;
	buf_len = 0;
	recv_buf = calloc(1, buf_size);

	while(run)
	{
		memset(&epoll_obj.event, 0, sizeof(struct epoll_event));

		if((epoll_obj.result = epoll_wait(epoll_obj.epoll, &epoll_obj.event, 1 , epoll_obj.timeout)) == 0)
			continue;
		else if(epoll_obj.event.data.fd != client_set.sock || epoll_obj.event.events != EPOLLIN)
			continue;

		while(true)
		{
			if(!run)
				break;

			recv_result = 0;

			if((recv_result = recv(client_set.sock, &recv_buf[buf_len], buf_size - buf_len, 0)) == -1)
			{
				if(errno == EAGAIN || errno == EWOULDBLOCK)
					break;

				sem_wait(print_sem);

				fprintf(stderr, "ERROR: client1 recv failed! %s\n", strerror(errno));

				sem_post(print_sem);

				run = false;

				break;
			}
			else
				buf_len += recv_result;

			if(buf_len == buf_size)
			{
				buf_size += 4096;
				recv_buf = realloc(recv_buf, buf_size);

				continue;
			}

			while(true)
			{
				if(!memcmp(recv_buf, "quit", 4))
				{
					run = false;
					break;
				}

				if(!element_count && buf_len > sizeof(uint64_t))
					element_count = *(uint64_t *) recv_buf;

				if(element_count && buf_len >= sizeof(uint64_t) + element_count * sizeof(int32_t))
					element_array = (int32_t *) &((uint64_t *) recv_buf)[1];
				else
					break;

				if(element_array)
				{
					double mean = 0;

					for(uint64_t i = 0; i < element_count; i++)
						mean = mean + ((element_array[i] - mean) / (i + 1));

					sem_wait(print_sem);

					fprintf(stdout, "client1: mean %g\n", mean);

					//printf("count %lu\n", element_count);
					//for(uint64_t i = 0; i < element_count; i++)
					//	printf("%lu, %d\n", i, element_array[i]);
					//printf("\n");

					sem_post(print_sem);
				}

				if(buf_len == sizeof(uint64_t) + element_count * sizeof(int32_t))
				{
					free(recv_buf);
					recv_buf = NULL;

					buf_size = 4096;
					buf_len = 0;
					recv_buf = calloc(1, buf_size);

					element_count = 0;
					element_array = NULL;

					break;
				}
				else
				{
					size_t temp_len = buf_len - (sizeof(uint64_t) + element_count * sizeof(int32_t));
					char *temp_buf = calloc(1, temp_len);
					memcpy(temp_buf, &recv_buf[sizeof(uint64_t) + element_count * sizeof(int32_t)], temp_len);

					buf_size = buf_len = temp_len;
					free(recv_buf);
					recv_buf = temp_buf;
					temp_buf = NULL;

					element_count = 0;
					element_array = NULL;
				}
			}
		}
	}

	socket_dest(epoll_obj.epoll, &client_set);
	epoll_dest(&epoll_obj);

	sem_wait(print_sem);

	fprintf(stdout, "client1 porecss terminated\n");

	sem_post(print_sem);

	sem_post(client1_sem);

	return;
}
