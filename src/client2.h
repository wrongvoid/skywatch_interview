#ifndef _CLIENT2_H_
#define _CLIENT2_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <stdint.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <semaphore.h>

void client2_func(void);

#endif
