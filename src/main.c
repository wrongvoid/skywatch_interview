#include "main.h"

sem_t *print_sem = NULL;
sem_t *server_sem = NULL;
sem_t *client1_sem = NULL;
sem_t *client2_sem = NULL;
sem_t *client3_sem = NULL;
sem_t *client3_write = NULL;
sem_t *client3_read = NULL;

int main(int argc, char *argv[])
{
	pid_t server_pid = -1;
	pid_t client1_pid = -1;
	pid_t client2_pid = -1;
	pid_t client3_pid = -1;

	sem_unlink("client3_read");
	sem_unlink("client3_write");
	sem_unlink("client3_sem");
	sem_unlink("client2_sem");
	sem_unlink("client1_sem");
	sem_unlink("server_sem");
	sem_unlink("print_sem");

	print_sem = sem_open("print_sem", O_CREAT, 0666, 1);
	server_sem = sem_open("server_sem", O_CREAT, 0666, 1);
	client1_sem = sem_open("client1_sem", O_CREAT, 0666, 1);
	client2_sem = sem_open("client2_sem", O_CREAT, 0666, 1);
	client3_sem = sem_open("client3_sem", O_CREAT, 0666, 1);
	client3_write = sem_open("client3_write", O_CREAT, 0666, 1);
	client3_read = sem_open("client3_read", O_CREAT, 0666, 0);

	if((server_pid = fork()) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "server fork failed!\n");

		sem_post(print_sem);

		exit(1);
	}
	else if(server_pid == 0)
	{
		client1_pid = -1;
		client2_pid = -1;
		client3_pid = -1;

		server_func();

		exit(0);
	}

	if((client1_pid = fork()) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "client1 fork failed!\n");

		sem_post(print_sem);

		exit(1);
	}
	else if(client1_pid == 0)
	{
		server_pid = -1;
		client2_pid = -1;
		client3_pid = -1;

		client1_func();

		exit(0);
	}

	if((client2_pid = fork()) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "client2 fork failed!\n");

		sem_post(print_sem);

		exit(1);
	}
	else if(client2_pid == 0)
	{
		server_pid = -1;
		client1_pid = -1;
		client3_pid = -1;

		client2_func();

		exit(0);
	}

	if((client3_pid = fork()) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "client3 fork failed!\n");

		sem_post(print_sem);

		exit(1);
	}
	else if(client3_pid == 0)
	{
		server_pid = -1;
		client1_pid = -1;
		client2_pid = -1;

		client3_func();

		exit(0);
	}

	waitpid(server_pid, NULL, 0);
	waitpid(client1_pid, NULL, 0);
	waitpid(client2_pid, NULL, 0);
	waitpid(client3_pid, NULL, 0);

	sem_close(client3_read);
	sem_close(client3_write);
	sem_close(client3_sem);
	sem_close(client2_sem);
	sem_close(client1_sem);
	sem_close(server_sem);
	sem_close(print_sem);

	sem_unlink("client3_read");
	sem_unlink("client3_write");
	sem_unlink("client3_sem");
	sem_unlink("client2_sem");
	sem_unlink("client1_sem");
	sem_unlink("server_sem");
	sem_unlink("print_sem");

	return 0;
}
