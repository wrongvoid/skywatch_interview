#ifndef _CLIENT1_H_
#define _CLIENT1_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <stdint.h>

#include <semaphore.h>

#include "socket.h"

void client1_func(void);

#endif
