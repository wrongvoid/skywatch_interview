#ifndef _SERVER_H_
#define _SERVER_H_

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

#include <semaphore.h>

#include "socket.h"

void server_func(void);

inline static void bubble_sort(int32_t *array, uint64_t count)
{
	int32_t temp = 0;

	for(uint64_t i = 0; i < count; i++)
		for(uint64_t j = 0; j < i; j++)
			if(array[j] > array[i])
			{
				temp = array[j];
				array[j] = array[i];
				array[i] = temp;
			}

	return;
}

#endif
