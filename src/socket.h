#ifndef _SOCKET_H_
#define _SOCKET_H_

#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <sys/epoll.h>

#define DEF_IP "127.0.0.1"
#define DEF_PORT 9000

struct socket_set
{
	int sock;
	struct sockaddr_in local_in;
	struct sockaddr_in remote_in;
	socklen_t sin_len;
};

inline static void socket_init(struct socket_set * const set)
{
	set->sock = -1;
	memset(&set->local_in, 0, sizeof(struct sockaddr_in));
	memset(&set->remote_in, 0, sizeof(struct sockaddr_in));
	set->sin_len = sizeof(struct sockaddr_in);

	return;
}

inline static void socket_dest(const int epoll_obj, struct socket_set * const set)
{
	epoll_ctl(epoll_obj, EPOLL_CTL_DEL, set->sock, NULL);
	close(set->sock);
	set->sock = -1;
	memset(&set->local_in, 0, sizeof(struct sockaddr_in));
	memset(&set->remote_in, 0, sizeof(struct sockaddr_in));
	set->sin_len = 0;

	return;
}

struct epoll_set
{
	int epoll;
	int result;
	int timeout;
	struct epoll_event event;
	int flag;
};

inline static bool epoll_init(struct epoll_set * const set)
{
	set->epoll = -1;
	set->result = 0;
	set->timeout = 0;
	memset(&set->event, 0, sizeof(struct epoll_event));
	set->flag = 0;

	if((set->epoll = (epoll_create1(0))) == -1)
		return false;

	return true;
}

inline static void epoll_dest(struct epoll_set * const set)
{
	close(set->epoll);
	set->epoll = -1;
	set->result = 0;
	set->timeout = 0;
	memset(&set->event, 0, sizeof(struct epoll_event));
	set->flag = 0;

	return;
}

#endif
