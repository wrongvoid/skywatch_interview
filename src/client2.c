#include "client2.h"

extern sem_t *print_sem;
extern sem_t *client2_sem;

void client2_func(void)
{
	//client2 member
	bool run = true;

	int client2_fd = -1;

	time_t t1 = 0, t2 = 0;

	size_t buf_size = 0;
	size_t buf_len = 0;
	char *recv_buf = NULL;

	ssize_t recv_result = 0;

	uint64_t element_count = 0;
	int32_t *element_array = NULL;

	//client2 prepare
	time(&t1);

	while(time(&t2) <=  t1 + 1 && (client2_fd = open("/tmp/interview_pipe", O_RDONLY)) == -1)
		;

	if(client2_fd == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: client2 open file failed! %s\n", strerror(errno));

		sem_post(print_sem);

		return;
	}

	sem_wait(client2_sem);

	sem_wait(print_sem);

	fprintf(stdout, "client2 porecss ready\n");

	sem_post(print_sem);

	buf_size = 4096;
	buf_len = 0;
	recv_buf = calloc(1, buf_size);

	while(run)
	{
		recv_result = 0;

		if((recv_result = read(client2_fd, &recv_buf[buf_len], buf_size - buf_len)) == -1)
		{
			sem_wait(print_sem);

			fprintf(stderr, "ERROR: client2 read failed! %s\n", strerror(errno));

			sem_post(print_sem);

			run = false;

			continue;
		}
		else if(recv_result == 0)
		{
			if(buf_len == buf_size)
			{
				buf_size += 4096;
				recv_buf = realloc(recv_buf, buf_size);
			}

			continue;
		}
		else
			buf_len += recv_result;

		while(true)
		{
			if(!memcmp(recv_buf, "quit", 4))
			{
				run = false;
				break;
			}

			if(!element_count && buf_len > sizeof(uint64_t))
				element_count = *(uint64_t *) recv_buf;

			if(element_count && buf_len >= sizeof(uint64_t) + element_count * sizeof(int32_t))
				element_array = (int32_t *) &((uint64_t *) recv_buf)[1];
			else
				break;

			if(element_array)
			{
				sem_wait(print_sem);

				if(element_count % 2)
					fprintf(stdout, "client2: median %d\n", element_array[element_count / 2]);
				else
					fprintf(stdout, "client2: median %g\n", ((double) element_array[element_count / 2 - 1] + (double) element_array[element_count / 2]) / 2);

				//printf("count %lu\n", element_count);
				//for(uint64_t i = 0; i < element_count; i++)
				//	printf("%lu, %d\n", i, element_array[i]);
				//printf("\n");

				sem_post(print_sem);
			}

			if(buf_len == sizeof(uint64_t) + element_count * sizeof(int32_t))
			{
				free(recv_buf);
				recv_buf = NULL;

				buf_size = 4096;
				buf_len = 0;
				recv_buf = calloc(1, buf_size);

				element_count = 0;
				element_array = NULL;

				break;
			}
			else
			{
				size_t temp_len = buf_len - (sizeof(uint64_t) + element_count * sizeof(int32_t));
				char *temp_buf = calloc(1, temp_len);
				memcpy(temp_buf, &recv_buf[sizeof(uint64_t) + element_count * sizeof(int32_t)], temp_len);

				buf_size = buf_len = temp_len;
				free(recv_buf);
				recv_buf = temp_buf;
				temp_buf = NULL;

				element_count = 0;
				element_array = NULL;
			}
		}
	}

	close(client2_fd);
	client2_fd = -1;
	remove("/tmp/interview_pipe");

	sem_wait(print_sem);

	fprintf(stdout, "client2 porecss terminated\n");

	sem_post(print_sem);

	sem_post(client2_sem);

	return;
}
