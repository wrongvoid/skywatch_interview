#include "server.h"

extern sem_t *print_sem;
extern sem_t *server_sem;

extern sem_t *client3_write;
extern sem_t *client3_read;

void server_func(void)
{
	//server member
	struct socket_set server_set;
	struct epoll_set epoll_obj;

	//stdin member
	size_t stdin_size = 0;
	ssize_t stdin_len = 0;
	char *stdin_buf = NULL;
	bool check = true;

	uint64_t element_count = 0;
	int32_t *element_array = NULL;
	char *token = NULL;
	bool tail_flag = false;

	//client1 member
	struct socket_set client_set;

	time_t t1 = 0, t2 = 0;

	//client2 member
	int client2_fd = -1;

	ssize_t client2_result = 0;

	//client3 member
	int client3_fd = -1;

	char *client3_buf = NULL;

	size_t client3_send_size = 0;
	size_t client3_send_len = 0;

	//server prepare
	socket_init(&server_set);

	if(!epoll_init(&epoll_obj))
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server epoll create failed! %s\n", strerror(errno));

		sem_post(print_sem);

		exit(1);
	}

	if((server_set.sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server socket create failed! %s\n", strerror(errno));

		sem_post(print_sem);

		epoll_dest(&epoll_obj);

		exit(1);
	}

	if((epoll_obj.flag = fcntl(server_set.sock, F_GETFL, 0)) == -1 || fcntl(server_set.sock, F_SETFL, epoll_obj.flag |O_NONBLOCK ) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server socket set failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

	memset(&epoll_obj.event, 0, sizeof(struct epoll_event));
	epoll_obj.event.events = EPOLLIN | EPOLLET;
	epoll_obj.event.data.fd = server_set.sock;

	if(epoll_ctl(epoll_obj.epoll, EPOLL_CTL_ADD, server_set.sock, &epoll_obj.event) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server epoll set failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

	server_set.local_in.sin_family = AF_INET;
	server_set.local_in.sin_port = htons(DEF_PORT);
	inet_pton(AF_INET, DEF_IP, &server_set.local_in.sin_addr);

	if(bind(server_set.sock, (struct sockaddr *) &server_set.local_in, server_set.sin_len) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server socket bind interface failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

	if(listen(server_set.sock, 1) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server socket listen failed! %s\n", strerror(errno));

		sem_post(print_sem);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

	sem_wait(server_sem);

	sem_wait(print_sem);

	fprintf(stdout, "server porecss ready\n");

	sem_post(print_sem);

	//client1 prepare
	socket_init(&client_set);

	time(&t1);

	while(time(&t2) <=  t1 + 2)
	{
		memset(&epoll_obj.event, 0, sizeof(struct epoll_event));

		if((epoll_obj.result = epoll_wait(epoll_obj.epoll, &epoll_obj.event, 1 , epoll_obj.timeout)) == 0)
			continue;
		else if(epoll_obj.event.data.fd != server_set.sock || epoll_obj.event.events != EPOLLIN)
			continue;

		if((client_set.sock = accept(server_set.sock, (struct sockaddr *) &client_set.remote_in, &client_set.sin_len)) == -1)
		{
			sem_wait(print_sem);

			fprintf(stderr, "ERROR: server synchronize client1 failed! %s\n", strerror(errno));

			sem_post(print_sem);

			socket_dest(epoll_obj.epoll, &server_set);
			epoll_dest(&epoll_obj);

			exit(1);
		}

		memcpy(&client_set.local_in, &server_set.local_in, sizeof(struct sockaddr_in));

		break;
	}

//printf("111\n");
	//client2 prepare
	remove("/tmp/interview_pipe");

	if(mkfifo("/tmp/interview_pipe", 0666) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server synchronize client2 create file failed! %s\n", strerror(errno));

		sem_post(print_sem);

		send(client_set.sock, "quit", 4, 0);
		socket_dest(epoll_obj.epoll, &client_set);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}
	else if((client2_fd = open("/tmp/interview_pipe", O_WRONLY)) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server synchronize client2 open file failed! %s\n", strerror(errno));

		sem_post(print_sem);

		remove("/tmp/interview_pipe");

		send(client_set.sock, "quit", 4, 0);
		socket_dest(epoll_obj.epoll, &client_set);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

//printf("222\n");
	//client3 prepare
	shm_unlink("interview_shm");

	if((client3_fd = shm_open("interview_shm", O_CREAT | O_RDWR, 0666)) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server synchronize client3 open file failed! %s\n", strerror(errno));

		sem_post(print_sem);

		close(client2_fd);
		client2_fd = -1;
		remove("/tmp/interview_pipe");

		send(client_set.sock, "quit", 4, 0);
		socket_dest(epoll_obj.epoll, &client_set);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}
	else if(ftruncate(client3_fd, 4096) == -1)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server synchronize client3 file setting failed! %s\n", strerror(errno));

		sem_post(print_sem);

		close(client3_fd);
		client3_fd = -1;
		shm_unlink("interview_shm");

		close(client2_fd);
		client2_fd = -1;
		remove("/tmp/interview_pipe");

		send(client_set.sock, "quit", 4, 0);
		socket_dest(epoll_obj.epoll, &client_set);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}
	else if((client3_buf = mmap(NULL, 4096, PROT_WRITE, MAP_SHARED, client3_fd, 0)) == NULL)
	{
		sem_wait(print_sem);

		fprintf(stderr, "ERROR: server synchronize client3 buffer setting failed! %s\n", strerror(errno));

		sem_post(print_sem);

		close(client3_fd);
		client3_fd = -1;
		shm_unlink("interview_shm");

		close(client2_fd);
		client2_fd = -1;
		remove("/tmp/interview_pipe");

		send(client_set.sock, "quit", 4, 0);
		socket_dest(epoll_obj.epoll, &client_set);

		socket_dest(epoll_obj.epoll, &server_set);
		epoll_dest(&epoll_obj);

		exit(1);
	}

//printf("333\n");
	sem_wait(print_sem);

	fprintf(stdout, "all process synchronized\n");

	sem_post(print_sem);

	while(true)
	{
		stdin_size = 0;
		stdin_len = 0;
		free(stdin_buf);
		stdin_buf = NULL;
		check = true;

		element_count = 0;
		free(element_array);
		element_array = NULL;
		token = NULL;
		tail_flag = false;

		client3_send_size = 0;
		client3_send_len = 0;

		sem_wait(print_sem);

		fprintf(stdout, "server: enter test data or \"quit\" leave program:\n");

		sem_post(print_sem);

		if((stdin_len = getline(&stdin_buf, &stdin_size, stdin)) == -1)
		{
			sem_wait(print_sem);

			fprintf(stderr, "ERROR: server getline failed! %s\n", strerror(errno));

			sem_post(print_sem);

			stdin_size = 0;
			stdin_len = 0;
			free(stdin_buf);
			stdin_buf = NULL;

			break;
		}
		else
			stdin_buf[--stdin_len] = 0;

		if(stdin_len == 4 && !memcmp(stdin_buf, "quit", 4))
		{
			send(client_set.sock, "quit", 4, 0);
			socket_dest(epoll_obj.epoll, &client_set);

			client2_result = write(client2_fd, "quit", 4);
			close(client2_fd);
			client2_fd = -1;

			sem_wait(client3_write);
			memset(client3_buf, 0, 4096);
			client3_send_size = 4;
			memcpy(&client3_buf[0], &client3_send_size, sizeof(size_t));
			memcpy(&client3_buf[sizeof(size_t)], "quit", 4);
			sem_post(client3_read);
			munmap(client3_buf, 4096);
			close(client3_fd);
			client3_fd = -1;

			stdin_size = 0;
			stdin_len = 0;
			free(stdin_buf);
			stdin_buf = NULL;

			break;
		}

		for(size_t i = 0; i < stdin_len; i++)
			if((stdin_buf[i] < '0' || stdin_buf[i] > '9') && stdin_buf[i] != ' ' && stdin_buf[i] != '-')
			{
				sem_wait(print_sem);

				fprintf(stderr, "WARNING: illegal input: %s\n", stdin_buf);

				sem_post(print_sem);

				check = false;

				break;
			}

		if(!check)
			continue;

		token = strtok(stdin_buf, " ");

		while(token != NULL)
		{
			tail_flag = false;

			if(token[0] == '\000')
				tail_flag = true;

			element_count++;
			token = strtok(NULL, " ");
		}

		if(tail_flag)
			element_count--;

		if(element_count > INT32_MAX || (element_array = calloc(element_count, sizeof(int32_t))) == NULL)
		{
			sem_wait(print_sem);

			fprintf(stderr, "WARNING: exceeds system capability: %s\n", stdin_buf);

			sem_post(print_sem);

			continue;
		}

		token = stdin_buf;

		for(uint64_t i = 0; i < element_count; i++)
		{
			while(token[0] == '\000' || token[0] == ' ')
				token += 1;

			if(sscanf(token, "%d", &element_array[i]) != 1)
			{
				sem_wait(print_sem);

				fprintf(stderr, "WARNING: illegal input: %s\n", token);

				sem_post(print_sem);

				check = false;

				break;
			}
			token = memchr(token, '\000', stdin_len);
		}

		if(!check)
			continue;

		bubble_sort(element_array, element_count);

		//for(uint64_t i = 0; i < element_count; i++)
		//	printf("%lu, %d\n", i, element_array[i]);

		//send to client1
		send(client_set.sock, &element_count, sizeof(uint64_t), 0);
		send(client_set.sock, element_array, element_count * sizeof(int32_t), 0);

		//send to client2
		client2_result = write(client2_fd, &element_count, sizeof(uint64_t));
		client2_result = write(client2_fd, element_array, element_count * sizeof(int32_t));

		//send to client3
		sem_wait(client3_write);
		memset(client3_buf, 0, 4096);
		client3_send_size = sizeof(uint64_t);
		memcpy(&client3_buf[0], &client3_send_size, sizeof(size_t));
		memcpy(&client3_buf[sizeof(size_t)], &element_count, sizeof(uint64_t));
		sem_post(client3_read);

		client3_send_size = element_count * sizeof(int32_t);

		while(true)
		{
			size_t temp = client3_send_size - client3_send_len;

			if(temp > 4096 - sizeof(size_t))
			{
				temp = 4096 - sizeof(size_t);

				sem_wait(client3_write);
				memset(client3_buf, 0, 4096);
				memcpy(&client3_buf[0], &temp, sizeof(size_t));
				memcpy(&client3_buf[sizeof(size_t)], &((char *) element_array)[client3_send_len], temp);
				sem_post(client3_read);

				client3_send_len += temp;
			}
			else
			{
				sem_wait(client3_write);
				memset(client3_buf, 0, 4096);
				memcpy(&client3_buf[0], &temp, sizeof(size_t));
				memcpy(&client3_buf[sizeof(size_t)], &((char *) element_array)[client3_send_len], temp);
				sem_post(client3_read);

				client3_send_len += temp;

				break;
			}
		}

		printf("\n");
	}

	socket_dest(epoll_obj.epoll, &server_set);
	epoll_dest(&epoll_obj);

	sem_wait(print_sem);

	fprintf(stdout, "server porecss terminated\n");

	sem_post(print_sem);

	sem_post(server_sem);

	return;
}
