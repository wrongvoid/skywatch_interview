#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include <semaphore.h>

#include "server.h"
#include "client1.h"
#include "client2.h"
#include "client3.h"

#endif
