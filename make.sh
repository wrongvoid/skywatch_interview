#!/bin/bash

DIR=$(dirname 0)

rm  $DIR/interview
rm -rf $DIR/build

cmake -S $DIR/src -B $DIR/build -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_BUILD_TYPE=Release
cmake --build $DIR/build -j1

cp $DIR/build/interview $DIR
